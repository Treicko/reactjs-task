/*
*/

console.info('------ Ejercicios ------');
console.info('1.');
const users = [
    { id: 11, nombre: 'Adam', edad: 23, grupo: 'editor' },
    { id: 47, nombre: 'John', edad: 28, grupo: 'admin' },
    { id: 85, nombre: 'William', edad: 34, grupo: 'editor' },
    { id: 97, nombre: 'Oliver', edad: 28, grupo: 'admin' }
];

const resp = users.filter((user) => user.nombre.includes('ohn'));
console.info('resp: ', resp.map((elem) => elem.nombre));

// elementos con la letra 'ohn' el nombre
console.info('------------');
console.info('2.');
const euros = [29, 76, 41.85, 46.5];
// suma y promedio de los valores
const sum = euros.reduce((elem, sum) => sum += elem);
const avg = sum / euros.length;
console.info('sum: ', sum);
console.info('avg: ', avg);