console.info('------ Ejercicios ------');
console.info('1.');

/*
    Class Auto
    tipo = string
    anio = anio
    hasSoat = boolean
    getter y setter para todos
*/

class Auto {
    constructor(data) {
        this.tipo = data.tipo;
        this.anio = data.anio;
        this.hasSoat = data.hasSoat;
    }

    getTipo() {
        return this.tipo;
    }

    getAnio() {
        return this.anio;
    }

    getHasSoat() {
        return this.hasSoat;
    }

    setTipo(tipo) {
        this.tipo = tipo;
    }

    setAnio(anio) {
        this.anio = anio;
    }

    setHasSoat(hasSoat) {
        this.hasSoat = hasSoat;
    }
}

/*
    Class Toyota
    modelo = "Toyota praus"
*/

class Toyota extends Auto {
    constructor(data) {
        super(data);
        this.modelo = data.modelo;
    }

    getModelo() {
        return this.modelo;
    }

    setModelo(mnodelo) {
        this.modelo = this.modelo;
    }
}

/*
    toString() todos los atributos
*/

const myToyota = new Toyota({
    tipo: "asdas",
    anio: 1990,
    hasSoat: true,
    modelo: "Toyota praus"
})

console.info('resp: ', JSON.stringify(myToyota));

