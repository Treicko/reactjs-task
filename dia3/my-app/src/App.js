import React from 'react';
import Notas from './notas/Main';

function App() {
  return (
    <div className="App">
      <Notas></Notas>
    </div>
  );
}

export default App;
