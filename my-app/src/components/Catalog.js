import React, { Component } from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle, CardBody, CardText } from 'reactstrap';
import { items } from './items';

class Catalog extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            items
        };
    }

    onItemSelect(item) {
        this.setState({
            selectedItem: item
        })
    }

    renderItem(item) {
        if (item != null) {
            return (
                <Card className="col-5">
                    <CardImg width="100%"
                        src={item.image} alt={item.name} />
                    <CardBody>
                        <CardTitle>{item.name}</CardTitle>
                        <CardText>{item.description}</CardText>
                    </CardBody>
                </Card>
            );

        } else {
            return (
                <div/>
            );
        }
    }

    render() {
        var catalog = this.state.items.map((item, index) => {
            return (
                <div key={index} className="col-12 col-md-5 m-1">
                    <Card onClick={() => { this.onItemSelect(item) }}>
                        <CardImg width="100%" src={item.image} alt={item.name} />
                        <CardImgOverlay>
                            <CardTitle>{item.name}</CardTitle>
                        </CardImgOverlay>
                    </Card>
                </div>
            );
        });
        return (
            <div className="container">
                <div className="row">
                    {catalog}
                </div>
                <div className="row">
                    {this.renderItem(this.state.selectedItem)}
                </div>
            </div>
        );
    }

}

export default Catalog;